/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 15:36:28 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/18 17:16:27 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	unsigned char	*tmp;
	size_t			i;

	if (!s)
		return ;
	tmp = (unsigned char *) s;
	i = 0;
	while (i < n)
	{
		tmp[i] = '\0';
		i++;
	}
}
