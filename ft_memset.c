/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 21:35:31 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/19 21:43:23 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*tmp;
	size_t			i;

	if (!b)
		return (NULL);
	i = 0;
	tmp = (unsigned char *) b;
	while (i < len)
	{
		tmp[i] = c;
		i++;
	}
	return (tmp);
}
