/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/26 21:21:05 by cananwat          #+#    #+#             */
/*   Updated: 2022/03/01 21:24:10 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_cnt_word(char const *s, char c)
{
	size_t	i;
	int		f;

	i = 0;
	f = 0;
	while (*s)
	{
		if (*s != c && f == 0)
		{
			f = 1;
			i++;
		}
		else if (*s == c)
			f = 0;
		s++;
	}
	return (i);
}

char	*ft_cpy_word(char const *s, int start, int end)
{
	char	*word;
	int		i;

	word = (char *)malloc(sizeof(char) * (end - start + 1));
	if (!word)
		return (NULL);
	i = 0;
	while (start < end)
	{
		word[i] = s[start];
		i++;
		start++;
	}
	word[i] = '\0';
	return (word);
}

char	**ft_split(char const *s, char c)
{
	int		start;
	size_t	i;
	size_t	j;
	char	**word;

	word = (char **)malloc(sizeof(char *) * (ft_cnt_word(s, c) + 1));
	if (!word)
		return (NULL);
	i = 0;
	j = 0;
	start = -1;
	while (i < (ft_strlen(s) + 1))
	{
		if (start < 0 && s[i] != c)
			start = i;
		else if ((s[i] == c || !s[i]) && start >= 0)
		{
			word[j++] = ft_cpy_word(s, start, i);
			start = -1;
		}
		i++;
	}
	word[j] = '\0';
	return (word);
}
