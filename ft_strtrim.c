/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/26 15:11:31 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/26 21:02:09 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_char_exist(char const *set, char c)
{
	size_t	i;

	i = 0;
	while (set[i])
	{
		if (set[i] == c)
			return (1);
		i++;
	}
	return (0);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	s_index;
	size_t	e_index;
	size_t	i;
	char	*trim;

	s_index = 0;
	while (s1[s_index] && ft_char_exist(set, s1[s_index]))
		s_index++;
	e_index = ft_strlen(s1) - 1;
	while (e_index > s_index && ft_char_exist(set, s1[e_index]))
		e_index--;
	trim = (char *)malloc(sizeof(*s1) * (e_index - s_index + 2));
	if (!trim)
		return (NULL);
	i = 0;
	while (s_index < e_index + 1)
	{
		trim[i] = s1[s_index];
		i++;
		s_index++;
	}
	trim[i] = '\0';
	return (trim);
}
